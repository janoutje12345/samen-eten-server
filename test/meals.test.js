process.env.DB_DATABASE = process.env.DB_DATABASE || "test";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../server");
const jwt = require("jsonwebtoken");
const pool = require('../src/storage/database').testpool;
const assert = require("assert");

const config = require('../src/config/tracerConfig')
const logger = config.logger

chai.should();
chai.use(chaiHttp);
const { expect } = require("chai");

const CLEAR_DB =
  "SET FOREIGN_KEY_CHECKS = 0;" +
  "DELETE IGNORE FROM `user`;" +
  "DELETE IGNORE FROM `studenthome`;" +
  "DELETE IGNORE FROM `meal`;" +
  "SET FOREIGN_KEY_CHECKS = 1;";
const ALTER_DB =
  "ALTER TABLE `user` AUTO_INCREMENT = 1;" +
  "ALTER TABLE `studenthome` AUTO_INCREMENT = 1;" +
  "ALTER TABLE `meal` AUTO_INCREMENT = 1;";
const INSERT_DB_USER =
  "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)";
const INSERT_STUDENTHOME =
  "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`, `ID`) VALUES" +
  "('Princenhage', 'Princenhage', 11, 1,'4706RX','061234567891','Breda', 1)";

// Empty the db, reset auto-increment, add 1 user and add 1 studenthome

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`);
describe("Meals", function () {
  before((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        logger.error(`before CLEARING tables: ${err}`);
        done(err);
      } else {
        pool.query(ALTER_DB, (err, rows, fields) => {
          if (err) {
            logger.error(`before INSERTING USER tables: ${err}`);
            done(err);
          } else {
            pool.query(
              INSERT_DB_USER,
              ["firstname", "lastname", "mail@mail.com", "123", "secret"],
              (err, rows, fields) => {
                if (err) {
                  logger.error(`before INSERTING USER tables: ${err}`);
                  done(err);
                } else {
                  pool.query(
                    INSERT_STUDENTHOME,
                    [
                      "housename",
                      "streetname",
                      "2",
                      "1",
                      "1234AB",
                      "0612345678",
                      "Amsterdam",
                    ],
                    (err, rows, fields) => {
                      if (err) {
                        logger.error(
                          `before INSERTING STUDENTHOME tables: ${err}`
                        );
                        done(err);
                      } else {
                        done();
                      }
                    }
                  );
                }
              }
            );
          }
        });
      }
    });
  });
  describe("UC-301 Add meal", function () {
    it("TC-301-1 should return valid error when required value is not present", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/1/meal")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          // Name is missing
          Description: "Real Italian pizza!",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("Name is missing");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-301-2 should return valid error user is not logged in", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/1/meal")
        .set("authorization", "Bearer " + "nogoodjwt")
        .send({
          Name: "worst",
          Description: "worst met spruitjes",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 14.5,
          Allergies: "Worst",
          Ingredients: "Worst en spruitjes",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Not authorized");
          error.should.be.a("string");
          datetime.should.be.a("string");
          done();
        });
    });
    it("TC-301-3 should return the meal when the meal is added succesfully", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/1/meal")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          Name: "Pizza",
          Description: "Real Italian pizza!",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("result");
          let { result } = res.body;
          result.should.be
            .an("object")
            .that.has.all.keys(
              "Name",
              "Description",
              "CreatedOn",
              "OfferedOn",
              "Price",
              "Allergies",
              "Ingredients",
              "MaxParticipants",
              "ID",
              "StudenthomeID",
              "UserID"
            );

          done();
        });
    });
  });

  describe("UC-302 Update meal", function () {
    it("TC-302-1 accept when required value is not present", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          // Name is missing
          Description: "Real Italian pizza!",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("result");
          let { result } = res.body;
          result.should.be
            .an("object")
            .that.has.all.keys(
              "Name",
              "Description",
              "CreatedOn",
              "OfferedOn",
              "Price",
              "Allergies",
              "Ingredients",
              "MaxParticipants",
              "ID",
              "StudenthomeID",
              "UserID"
            );
          done();
        });
    });
    it("TC-302-2 should return valid error when user is not logged in", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + "invalid jwt")
        .send({
          Name: "Pizza",
          Description: "Real Italian pizza!",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Not authorized");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC-302-3 should return valid error user when user is not owner of the meal", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
        .send({
          Name: "Pizza",
          Description: "Real Italian pizza!",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("You do not have access to this item");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-302-4 should return a valid error when the meal doesn't exist", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/0")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          Name: "Pizza",
          Description: "Real Italian pizza!",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("house does not exist or meal does not exist");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-302-5 should return the meal when the meal is updated succesfully", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          Name: "Pizza",
          Description: "Real Italian pizza!",
          OfferedOn: "2030-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 1,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("result");
          let { result } = res.body;
          result.should.be
            .an("object")
            .that.has.all.keys(
              "Name",
              "Description",
              "CreatedOn",
              "OfferedOn",
              "Price",
              "Allergies",
              "Ingredients",
              "MaxParticipants",
              "ID",
              "StudenthomeID",
              "UserID"
            );

          done();
        });
    });
  });

  describe("UC-303 Get meal overview", function () {
    it("TC 303-1 should return a list of meals", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/1/meal")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(1);
          expect(res.body.result[0]).to.have.keys(
            "Allergies",
            "CreatedOn",
            "Description",
            "ID",
            "Ingredients",
            "MaxParticipants",
            "Name",
            "OfferedOn",
            "Price",
            "StudenthomeID",
            "UserID"
          );
          done();
        });
    });
  });

  describe("UC-304 Get meal details", function () {
    it("TC 304-1 should return a valid error when meal doesn't exist", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/1/meal/0")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("house does not exist or meal does not exist");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC 304-2 should return the details of a meal when meal exists", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/1/meal/1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("object");
          expect(res.body.result).to.have.keys(
            "Allergies",
            "CreatedOn",
            "Description",
            "ID",
            "Ingredients",
            "MaxParticipants",
            "Name",
            "OfferedOn",
            "Price",
            "StudenthomeID",
            "UserID"
          );
        });
      done();
    });
  });

  describe("UC-305 Delete meal", function () {
    it("TC-305-2 should return valid error when user is not logged in", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + "Invalid token")
        .send({
          Name: "Pizza",
          Description: "Real Italian pizza!",
          OfferedOn: "2021-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Not authorized");
          error.should.be.a("string");
          datetime.should.be.a("string");

        });
      done();
    });
    it("TC-305-3 should return valid error user when user is not owner of the meal", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
        .send({
          Name: "Pizza",
          Description: "Real Italian pizza!",
          OfferedOn: "2021-5-20 13:49:27",
          Price: 4.5,
          Allergies: "Gluten",
          Ingredients: "Tomato sauce, Pizza dough, Mozzarella",
          MaxParticipants: 4,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("You do not have access to this item");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC-305-4 should return a valid error when the meal doesn't exist", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/1/meal/0")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("No meal with id 0 found in studenthome with id 1");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC-305-5 should return the meal when the meal is deleted succesfully", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("result", "deletedItem");
          let { deletedItem } = res.body;
          deletedItem.should.be
            .an("object")
            .that.has.all.keys(
              "Allergies",
              "CreatedOn",
              "Description",
              "ID",
              "Ingredients",
              "MaxParticipants",
              "Name",
              "OfferedOn",
              "Price",
              "StudenthomeID",
              "UserID"
            );
        });
      done();
    });
  });

  describe("UC-401 sign up for meal", function () {
    it("TC-401-1 should return valid error when user is not logged in", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + "invalid jwt")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Not authorized");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC-401-2 should show result when signing up", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(1);
          expect(res.body.result[0]).to.have.keys(
            "MealID",
            "SignedUpOn",
            "StudenthomeID",
            "UserID"
          );
        });
      done();
    });
    it("TC-401-3 should tell when already seated", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(409);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("You are already seated");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC-401-4 should not seat more than limit", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(409);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("All seats are already taken");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC-401-5 should return valid error when meal does not exist", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/500")
        .set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("house does not exist or meal does not exist");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
  });

  describe("UC-403 list of participants", function () {
    it("TC-403-1 should show participants", (done) => {
      chai
        .request(server)
        .get("/api/meal/1/participants")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(1);
          expect(res.body.result[0]).to.have.keys(
            "UserID",
            "StudenthomeID",
            "MealID",
            "SignedUpOn"
          );
        });
      done();
    });
    it("TC-403-2 should return valid error when meal does not exist", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/500")
        .set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Could not get the list of people for the given mealId");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC-403-2 should return valid error when there are no participants", (done) => {
      chai
        .request(server)
        .get("/api/meal/2/participants")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Could not get the list of people for the given mealId");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
  });
  describe("UC-404 details from participant", function () {
    it("TC 404-1 should return the details of participant when exists", (done) => {
      chai
        .request(server)
        .get("/api/meal/1/participants/1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("object");
          expect(res.body.result).to.have.keys(
            "ID",
            "First_Name",
            "Email",
            "Student_Number"
          );
        });
      done();
    });
    it("TC 404-2 should return valid error when user is not seated at that table", (done) => {
      chai
        .request(server)
        .get("/api/meal/1/participants/80")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("User is not found at this table");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC 404-3 should return valid error when table had no participants", (done) => {
      chai
        .request(server)
        .get("/api/meal/500/participants/80")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("There are no people at this table");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
    it("TC 404-4 should return logs", (done) => {
      chai
        .request(server)
        .get("/api/meal/log")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(422);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("log till you drop");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
      done();
    });
  })
  describe("UC-402 sign off for meal", function () {
    it("TC-402-1 should show result when signing off", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/1/meal/1/signoff")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(0);
          
        });
      done();
    });
    

  });
});
