process.env.DB_DATABASE = process.env.DB_DATABASE || "test";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../server");
const jwt = require("jsonwebtoken");
const pool = require('../src/storage/database').testpool;
const assert = require("assert");

const config = require('../src/config/tracerConfig')
const logger = config.logger

chai.should();
chai.use(chaiHttp);
const { expect } = require("chai");

const INSERT_USER =
    "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
    '(1, "first", "last", "name@server.nl","1234567", "secret");';

const CLEAR_DB =
    "SET FOREIGN_KEY_CHECKS = 0;" +
    "DELETE IGNORE FROM `user`;" +
    "DELETE IGNORE FROM `studenthome`;" +
    "DELETE IGNORE FROM `meal`;" +
    "SET FOREIGN_KEY_CHECKS = 1;";
const ALTER_DB =
    "ALTER TABLE `user` AUTO_INCREMENT = 1;" +
    "ALTER TABLE `studenthome` AUTO_INCREMENT = 1;" +
    "ALTER TABLE `meal` AUTO_INCREMENT = 1;";
const INSERT_DB_USER =
    "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)";
const INSERT_STUDENTHOME =
    "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`, `ID`) VALUES" +
    "('Princenhage', 'Princenhage', 11, 1,'4706RX','061234567891','Breda', 1)";

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`);

describe("User", () => {
    before((done) => {
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEARING tables: ${err}`);
                done(err);
            } else {
                pool.query(INSERT_USER, (err, rows, fields) => {
                    if (err) {
                        logger.error(`before ALTER tables: ${err}`);
                        done(err);
                    } else {
                        done();
                    }
                });
            }
        });
    });
    describe("UC-501 check user", function () {
        it("TC-501-1 should return valid error when no user is found", (done) => {
            chai
                .request(server)
                .get("/api/user/500")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))

                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(422);
                    res.should.be.an("object");

                    res.body.should.be
                        .an("object")
                        .that.has.all.keys("error", "message", "datetime");

                    let { message, error, datetime } = res.body;
                    message.should.be
                        .a("string")
                        .that.equals("user does not exist");
                    error.should.be.a("string");
                    datetime.should.be.a("string");
                });
            done();
        });
        it("TC-501-2 should return user when user is found", (done) => {
            chai
                .request(server)
                .get("/api/user/1")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))

                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(200);
                    res.should.be.an("object");

                    res.body.should.be
                        .an("object")
                        .that.has.all.keys(
                            "ID",
                            "First_Name",
                            "Email",
                            "Student_Number",
                        );
                });
            done();
        });
        it("TC-501-3 should return user ", (done) => {
            chai
                .request(server)
                .get("/api/user/me")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))

                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(200);
                    res.should.be.an("object");

                    res.body.should.be
                        .an("object")
                        .that.has.all.keys(
                            "ID",
                            "First_Name",
                            "Email",
                            "Student_Number",
                        );
                });
            done();
        });
    });
    describe("UC-502 change user", function () {
        it("TC-502-1 should return updated user", (done) => {
            chai
                .request(server)
                .put("/api/user/update")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    firstname: "tester",
                    lastname: "Blom",
                    email: "test@test.com",
                    password: "secret!!!!1111",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(200);
                    res.should.be.an("object");

                    res.body.should.be
                        .an("object")
                        .that.has.all.keys(
                            "ID",
                            "First_Name",
                            "Email",
                            "Student_Number",
                        );

                    let { ID, First_Name, Email, Student_Number } = res.body;
                    ID.should.be.a("number");
                    First_Name.should.be.a("string").that.equals("tester");
                    lastName.should.be.a("string").that.equals("Blom");
                    Student_Number.should.be.a("string");
                    Email.should.be.a("string").that.equals("test@test.com");

                });
            done();
        });
        it("TC-502-2 should return valid error when user is not logged in", (done) => {
            chai
                .request(server)
                .put("/api/user/update")

                .send({
                    firstname: "tester",
                    lastname: "Blom",
                    email: "test@test.com",
                    password: "secret!!!!1111",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(401);
                    res.should.be.an("object");
                    res.body.should.be
                        .an("object")
                        .that.has.all.keys("error", "message", "datetime");
                    let { message, error, datetime } = res.body;
                    message.should.be.a("string").that.equals("Authorization header missing!");
                    error.should.be.a("string");
                    datetime.should.be.a("string");

                });
            done();
        });
    });
    describe("UC-503 delete user", function () {
        it("TC-503-1 should return deleted user", (done) => {
            chai
                .request(server)
                .delete("/api/user/me")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .send({
                    firstname: "tester",
                    lastname: "Blom",
                    email: "test@test.com",
                    password: "secret!!!!1111",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(200);
                    res.body.should.be
                        .an("object")
                        .that.has.all.keys("result");
                    let { deleted } = res.body;
                    deleted.should.be
                        .an("object")
                        .that.has.all.keys(
                            "ID",
                            "First_Name",
                            "Last_Name",
                            "Email",
                            "Student_Number",
                        );

                    let { ID, First_Name, Email,Last_Name, Student_Number } = deleted;
                    ID.should.be.a("number");
                    First_Name.should.be.a("string").that.equals("tester");
                    Last_Name.should.be.a("string").that.equals("Blom");
                    Student_Number.should.be.a("string");
                    Email.should.be.a("string").that.equals("test@test.com");

                });
            done();
        });
        
        it("TC-503-2 should return valid error when user is not logged in", (done) => {
            chai
                .request(server)
                .delete("/api/user/me")
                .send({
                    firstname: "tester",
                    lastname: "Blom",
                    email: "test@test.com",
                    password: "secret!!!!1111",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(401);
                    res.should.be.an("object");
                    res.body.should.be
                        .an("object")
                        .that.has.all.keys("error", "message", "datetime");
                    let { message, error, datetime } = res.body;
                    message.should.be.a("string").that.equals("Authorization header missing!");
                    error.should.be.a("string");
                    datetime.should.be.a("string");

                });
            done();
        });
    });
});
