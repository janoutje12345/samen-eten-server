process.env.DB_DATABASE = process.env.DB_DATABASE || "test";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../server");
const jwt = require("jsonwebtoken");
const pool = require('../src/storage/database').testpool;
const assert = require("assert");

const config = require('../src/config/tracerConfig')
const logger = config.logger

chai.should();
chai.use(chaiHttp);
const { expect } = require("chai");

const CLEAR_DB =
  "DELETE IGNORE FROM `studenthome`;" +
  "DELETE IGNORE FROM `user`;" +
  "DELETE IGNORE FROM `meal`;" +
  "DELETE IGNORE FROM `participants`;" +
  "DELETE IGNORE FROM `home_administrators`;";
const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;";
const INSERT_USER =
  "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
  '(1, "first", "last", "name@server.nl","1234567", "secret");';
const INSERT_TWO_STUDENTHOMES =
  "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES" +
  "('Prins', 'Princenhage', 11, 1,'4706RX','061234567891','Breda')," +
  "('Haagje 23', 'Haagdijk', 4, 1, '4706RX','061234567891','Amsterdam')";
const GET_ALL_STUDENTHOMEID = "SELECT `ID` FROM `studenthome`;";
//Will be assigned an existing studenthomeId later
let existingStudenthomeID;

// Empty the db before testing

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`);
describe("Studenthome", () => {
  before((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        logger.error(`before CLEARING tables: ${err}`);
        done(err);
      } else {
        pool.query(INSERT_USER, (err, rows, fields) => {
          if (err) {
            logger.error(`before ALTER tables: ${err}`);
            done(err);
          } else {
            done();
          }
        });
      }
    });
  });
  describe("UC-201 Add studenthome", function () {
    it("TC-201-1 should return valid error when required value is not present", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          // Name is missing
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("name is missing");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-201-2 should return valid error when postal code is not valid", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Pino",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "123ABC",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("postalcode not correct, needs format: '1234AB', but got 123ABC");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-201-3 should return valid error when telephone number is not valid", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Pino",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "061234567",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("phonenumber not correct, needs format: '0612345678', but got 061234567");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-201-4 should return valid error when studenthome already exists in database", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Pino",
          streetName: "Sesamstraat",
          houseNumber: 12,
          city: "Amsterdam",
          postalcode: "4321AB",
          phonenumber: "0612345678",
        })
        .end(() => {
          chai
            .request(server)
            .post("/api/studenthome")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .send({
              name: "Huis van Ernie",
              streetName: "Sesamstraat",
              houseNumber: 12,
              city: "Amsterdam",
              postalcode: "4321AB",
              phonenumber: "0687654321",
            })
            .end((err, res) => {
              assert.ifError(err);
              res.should.have.status(400);
              res.should.be.an("object");

              res.body.should.be
                .an("object")
                .that.has.all.keys("error", "message", "datetime");

              let { message, error, datetime } = res.body;
              message.should.be
                .a("string")
                .that.equals("address already in use");
              error.should.be.a("string");
              datetime.should.be.a("string");

              
            });
            
        });
        done();
    });
    it("TC-201-5 should return valid error when user is not logged in", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .set("authorization", "Bearer " + "invalid jwt")
        .send({
          name: "Huis van Pino",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Not authorized");
          error.should.be.a("string");
          datetime.should.be.a("string");
          
        });
        done();
    });
    it("TC-201-6 should return the studenthome when studenthome is added successfully", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Pino",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("result");
          let { result } = res.body;
          result.should.be
            .an("object")
            .that.has.all.keys(
              "name",
              "streetName",
              "House_Nr",
              "city",
              "Postal_Code",
              "Telephone",
              "id"
            );

          let { name, streetName, House_Nr, city, Postal_Code, Telephone, id } =
            result;
          name.should.be.a("string");
          streetName.should.be.a("string");
          House_Nr.should.be.a("number");
          city.should.be.a("string");
          Postal_Code.should.be.a("string");
          Telephone.should.be.a("string");
          id.should.be.a("number");

          
        });
        done();
    });
  });

  describe("UC-202 Studenthome overview", function () {
    before((done) => {
      pool.query(CLEAR_STUDENTHOME_TABLE, (err, rows, fields) => {
        if (err) {
          logger.error(`before CLEARING studenthome UC 202: ${err}`);
          done(err);
        } else {
          done();
        }
      });
    });
    it("TC-202-1 should return an empty JSON array", (done) => {
      chai
        .request(server)
        .get("/api/studenthome")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(0);

          
        });
        done();
    });
    it("TC-202-2 should return an array with 2 studenthomes", (done) => {
      pool.query(INSERT_TWO_STUDENTHOMES, (err, rows, fields) => {
        if (err) {
          logger.error(`before CLEARING tables: ${err}`);
          done(err);
        }
      });
      chai
        .request(server)
        .get("/api/studenthome")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(2);
          expect(res.body.result[0]).to.have.keys(
            "ID",
            "Name",
            "Address",
            "House_Nr",
            "UserID",
            "Postal_Code",
            "Telephone",
            "City"
          );

          expect(res.body.result[1]).to.have.keys(
            "ID",
            "Name",
            "Address",
            "House_Nr",
            "UserID",
            "Postal_Code",
            "Telephone",
            "City"
          );
          
        });
        done();
    });
    it("TC-202-3 should return valid error when city query doesn't return any studenthomes", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?city=test")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("No studenthomes found");
          error.should.be.a("string");
          datetime.should.be.a("string");
          
        });
        done();
    });
    it("TC-202-4 should return valid error when city query doesn't return any studenthomes", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?name=test")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("No studenthomes found");
          error.should.be.a("string");
          datetime.should.be.a("string");
          
        });
        done();
    });
    it("TC-202-5 should return studenthome that matches the city query", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?city=Breda")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(1);
          expect(res.body.result[0]).to.include({ City: "Breda" });
          
        });
        done();
    });
    it("TC-202-6 should return studenthomes that start with the name query", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?name=Haa")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("array");
          expect(res.body.result).to.have.a.lengthOf(1);
          expect(res.body.result[0].Name).to.match(/^Haa/i);
          
        });
        done();
    });
  });

  describe("UC-203 Get studenthome details", function () {
    before((done) => {
      pool.query(GET_ALL_STUDENTHOMEID, (err, rows, fields) => {
        if (err) {
          logger.error(`before CLEARING tables: ${err}`);
          done(err);
        } else {
          existingStudenthomeID = rows[0].ID;
          done();
        }
      });
    });
    it("TC-203-1 should return valid error when studenthome id doesn't exist in db", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/0")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("house does not exist");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-203-2 should return the requested studenthome", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/" + existingStudenthomeID)
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.result.should.be.an("object");
          expect(res.body.result).to.have.keys(
            "ID",
            "Name",
            "Address",
            "House_Nr",
            "UserID",
            "Postal_Code",
            "Telephone",
            "City"
          );
          
        });
        done();
    });
  });

  describe("UC-204 Update studenthome", function () {
    it("TC-204-1 should accept when required value is not present", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          // Name is missing
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Sesam",
          postalcode: "1234AB",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");
  
            res.body.should.be.an("object").that.has.all.keys("result");
            let { result } = res.body;
            result.should.be
              .an("object")
              .that.has.all.keys(
                "name",
                "streetName",
                "House_Nr",
                "city",
                "Postal_Code",
                "Telephone",
                "id"
              );
  
            let { name, streetName, House_Nr, city, Postal_Code, Telephone, id } =
              result;
            name.should.be.a("string");
            streetName.should.be.a("string");
            House_Nr.should.be.a("number");
            city.should.be.a("string");
            Postal_Code.should.be.a("string");
            Telephone.should.be.a("string");
            id.should.be.a("number");
  
            
          });
          done();
    });
    it("TC-204-2 should return valid error when postal code is not valid", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Ernie",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "123ABC",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("postalcode not correct, needs format: '1234AB', but got 123ABC");
          error.should.be.a("string");
          datetime.should.be.a("string");

          
        });
        done();
    });
    it("TC-204-3 should return valid error when telephone number is not valid", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Ernie",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "061234567",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("phonenumber not correct, needs format: '0612345678', but got 061234567");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-204-4 should return a valid error when studenthome doesn't exist in db", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/0")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Ernie",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("No studenthome with id 0 found!");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-204-5 should return valid error when user is not logged in", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + "invalid jwt")
        .send({
          name: "Huis van Pino",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "061234567",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Not authorized");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-204-6 should return the studenthome when studenthome is updated successfully", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .send({
          name: "Huis van Ernie",
          streetName: "Sesamstraat",
          houseNumber: 79,
          city: "Amsterdam",
          postalcode: "1234AB",
          phonenumber: "0612345678",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("result", "updatedItem");
          let { result, updatedItem } = res.body;
          updatedItem.should.be
            .an("object")
            .that.has.all.keys(
              "id",
              "name",
              "streetName",
              "House_Nr",
              "city",
              "Postal_Code",
              "Telephone",
              "userId"
            );

          let { name, streetName, House_Nr, city, Postal_Code, Telephone } =
            updatedItem;
          expect(name).to.equal("Huis van Ernie");
          expect(streetName).to.equal("Sesamstraat");
          expect(House_Nr).to.equal(79);
          expect(city).to.equal("Amsterdam");
          expect(Postal_Code).to.equal("1234AB");
          expect(Telephone).to.equal("0612345678");

        });
        done();
    });
    it("TC-204-7 should return the studenthome when studenthome is updated successfully", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("result", "updatedItem");
          let { result, updatedItem } = res.body;
          updatedItem.should.be
            .an("object")
            .that.has.all.keys(
              "id",
              "name",
              "streetName",
              "House_Nr",
              "city",
              "Postal_Code",
              "Telephone",
              "userId"
            );

          let { name, streetName, House_Nr, city, Postal_Code, Telephone } =
          updatedItem;
          expect(name).to.equal("Huis van Ernie");
          expect(streetName).to.equal("Sesamstraat");
          expect(House_Nr).to.equal(79);
          expect(city).to.equal("Amsterdam");
          expect(Postal_Code).to.equal("1234AB");
          expect(Telephone).to.equal("0612345678");

        });
        done();
    });
  });

  describe("UC-205 Delete studenthome", function () {
    it("TC-205-1 should return valid error when studenthome doesn't exist in the db", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/0")
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("house does not exist");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-205-2 should return valid error when user is not logged in", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + "invalid jwt")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");
          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("Not authorized");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-205-3 should return valid error when user is not the owner of the studenthome", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + jwt.sign({ id: 2 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("You do not have access to this item");
          error.should.be.a("string");
          datetime.should.be.a("string");
        });
        done();
    });
    it("TC-205-4 should return the studenthome when studenthome is successfully deleted", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/" + existingStudenthomeID)
        .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("result");
          let { result } = res.body;
          result.should.be
            .an("object")
            .that.has.all.keys(
              "ID",
              "Name",
              "Address",
              "House_Nr",
              "UserID",
              "Postal_Code",
              "Telephone",
              "City"
            );
        });
        done();
    });
  });
});
