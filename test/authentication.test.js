process.env.DB_DATABASE = process.env.DB_DATABASE || "test";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../server");
const jwt = require("jsonwebtoken");
const pool = require('../src/storage/database').testpool;
const assert = require("assert");

const config = require('../src/config/tracerConfig')
const logger = config.logger

chai.should();
chai.use(chaiHttp);
const { expect } = require("chai");

const CLEAR_DB = "DELETE IGNORE FROM `user`";
const ALTER_DB = "ALTER TABLE `user` AUTO_INCREMENT = 1;";
const INSERT_USER =
  "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
  '("first", "last", "name@server.nl","1234567", "secret123");';

describe("Authentication", () => {
  before((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        logger.error(`before CLEARING tables: ${err}`);
        done(err);
      } else {
        pool.query(ALTER_DB, (err, rows, fields) => {
          if (err) {
            logger.error(`before ALTER tables: ${err}`);
            done(err);
          } else {
            done();
          }
        });
      }
    });
  });
  describe("UC-101 Registration", () => {
    before((done) => {
      pool.query(INSERT_USER, (err, rows, fields) => {
        if (err) {
          logger.error(`before CLEARING tables: ${err}`);
          done(err);
        } else {
          done();
        }
      });
    });
    it("TC-101-1 should return valid error when required value is not present", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          // Firstname is missing
          lastname: "LastName",
          email: "test@test.nl",
          Student_Number: "1234567",
          password: "secret123",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("AssertionError [ERR_ASSERTION]: firstname must be a string.");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-101-2 should return valid error when email is not valid", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "Firstname",
          lastname: "LastName",
          // Mail has no '@'
          email: "testtesttest.nl",
          Student_Number: "1234567",
          password: "secret123",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("AssertionError [ERR_ASSERTION]: email not correct.");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-101-3 should return valid error when password is not valid", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "Firstname",
          lastname: "LastName",
          email: "test@test.nl",
          Student_Number: "1234567",
          // Password too short and no numbers in it
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals(
              "AssertionError [ERR_ASSERTION]: password needs to be 7 to 15 characters which contain at least one numeric digit and a special character."
            );
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-101-4 should return valid error when user already exists", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "Firstname",
          lastname: "LastName",
          // Email already exists
          email: "name@server.nl",
          Student_Number: "1234567",
          password: "secret!!!!1111",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("ER_DUP_ENTRY: Duplicate entry 'name@server.nl' for key 'Email'");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-101-5 should return userinfo with token when registrated successfully", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "Firstname",
          lastname: "Lastname",
          email: "test@server.nl",
          Student_Number: "1234567",
          password: "secret!!!!1111",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys(
              "id",
              "firstName",
              "lastName",
              "Student_Number",
              "emailAdress",
              "token"
            );

          let { id, firstName, lastName, Student_Number, emailAdress, token } =
            res.body;
          id.should.be.a("number");
          firstName.should.be.a("string").that.equals("Firstname");
          lastName.should.be.a("string").that.equals("Lastname");
          Student_Number.should.be.a("string").that.equals("1234567");
          emailAdress.should.be.a("string").that.equals("test@server.nl");
          token.should.be.a("string");

          done();
        });
    });
  });
  describe("UC-102 Login", () => {
    before((done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "first",
          lastname: "last",
          email: "mail@server.nl",
          Student_Number: "1234567",
          password: "secret!!!!1111",
        })
        .end((err, res) => {
          if (res) {
            done();
          } else {
            done(err);
          }
        });
    });
    it("TC-102-1 should return valid error when required value is not present", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          // Email is missing
          password: "secret!!!!1111",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("email must be a string.");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-102-2 should return valid error when email is not valid", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          // Mail has no '@'
          email: "testtest.com",
          password: "secret!!!!1111",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals("email not correct.");
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-102-3 should return valid error when password is not valid", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          // Password too short and no numbers in it
          email: "test@test.com",
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be
            .a("string")
            .that.equals(
              "password needs to be 7 to 15 characters which contain at least one numeric digit and a special character."
            );
          error.should.be.a("string");
          datetime.should.be.a("string");

          done();
        });
    });
    it("TC-102-4 should return valid error when user does not exist", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          firstname: "tester",
          lastname: "Blom",
          email: "test@test.com",
          password: "secret!!!!1111",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("error", "message", "datetime");

          let { message, error, datetime } = res.body;
          message.should.be.a("string").that.equals("User not found or password invalid.");
          error.should.be.a("string");
          datetime.should.be.a("string");
          done();
        });
    });
    it("TC-102-5 should return userinfo with token when logged in successfully", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "mail@server.nl",
          password: "secret!!!!1111",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys(
              "id",
              "firstName",
              "lastName",
              "Student_Number",
              "emailAdress",
              "token"
            );

          let { id, firstName, lastName, Student_Number, emailAdress, token } =
            res.body;
          id.should.be.a("number");
          firstName.should.be.a("string").that.equals("first");
          lastName.should.be.a("string").that.equals("last");
          Student_Number.should.be.a("string").that.equals("1234567");
          emailAdress.should.be.a("string").that.equals("mail@server.nl");
          token.should.be.a("string");

          done();
        });
    });
  });
});
