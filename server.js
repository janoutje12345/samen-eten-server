//npm run start-dev to use nodemon
const config = require('./src/config/tracerConfig')
const logger = config.logger

//app is a express object
const express = require('express')
const app = express()
app.use(express.json())
//database is connected
const database = require('./src/storage/database')
//house route is connected
const houseRoute = require('./src/routes/house-routes')
const mealRoute = require('./src/routes/meal-routes')
const validationRoute = require('./src/routes/authentication.routes')
const userRoute = require('./src/routes/user-routes')


const port = process.env.PORT || 3000

//console logging the request and request type
app.all('*', (req, res, next) => {
  logger.info("request: '" + req.method + " " + req.url + "' received")
  next()
})

//sending to routes
app.use('/api',validationRoute)
app.use('/api',houseRoute)
app.use('/api',mealRoute)
app.use('/api',userRoute)

app.get('/api/info', (req, res, next) => {
  logger.info('system info')
  //StudentnummerSonarqube URL
  let resValue = {
    Studentnaam : "Janou",
    Studentnummer: 2168565,
    Beschrijving: "Dit is de samen eten server, ontwikkeld door Janou, 1e jaars student op avans",
    Sonarqube_URL: 11111111
  }
  res.status = 200
  res.json(resValue)
})


//bad request catcher
app.all('*', (req, res, next) => {
  logger.info('Bad request called')
  next({ error: "request: '" + req.method + req.url + "' does not exist", errCode: 400 })
})

//error handler
app.use((error, req, res, next) => {
  logger.info("Errorhandler called")
  
  if(error.errCode != undefined){
    logger.warn(error.error)
    res.status(error.errCode).json({
      error: "Some error occured",
      //error has a text called error, that text is being returned
      message: error.error,
      datetime: new Date().toISOString()
    })
  }else{
    logger.warn(error.message)
    res.status(400).json({
      error: "Some error occured",
      message: error.message,
      datetime: new Date().toISOString()
    })
  }
})

app.listen(port, () => {
  logger.info(`Example app listening at http://localhost:${port}`)
})

module.exports= app
