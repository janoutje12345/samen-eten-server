const logLevel = process.env.LOGLEVEL || 'log'
//levels could be: .log, .trace, .debug, .info, .warn, .error
module.exports ={

    logger: require('tracer').console({
        format: [
            '<{{title}}> (in {{file}}) {{message}} ', 
          ],
        level: logLevel
    })


}