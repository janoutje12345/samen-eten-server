const mysql = require('mysql')
const logger = require('../config/config').logger
const dbconfig = require('../config/config').dbconfig
const dbTestconfig = require('../config/config').dbTestconfig

const pool = mysql.createPool(dbconfig)
const testpool = mysql.createPool(dbTestconfig)

pool.on('connection', function (connection) {
  //logger.trace('Database connection established')
})

pool.on('acquire', function (connection) {
  //logger.trace('Database connection aquired')
})

pool.on('release', function (connection) {
  //logger.trace('Database connection released')
})

//for testing

testpool.on('connection', function (connection) {
  logger.trace('Database connection established')
})

testpool.on('acquire', function (connection) {
  logger.trace('Database connection aquired')
})

testpool.on('release', function (connection) {
  logger.trace('Database connection released')
})


let query = (sqlQuery, sqlValues, callback) => {
  pool.getConnection(function (err, connection) {
    if (err) {
      
      callback(err.message, undefined)
    }
    if (connection) {
      connection.query(sqlQuery, sqlValues, (error, results, fields) => {
        connection.release()
        if (error) {
        
          callback(error.message, undefined)
        }
        if (results) {
          callback(undefined, results)
        }
      })
    }
  })
}

module.exports = { query, pool, testpool }
