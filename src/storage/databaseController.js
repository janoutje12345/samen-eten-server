const { get } = require("got");
const config = require('../config/tracerConfig')
const logger = config.logger
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connection = require("./database")


module.exports = {
    getAll(name, city, callback) {
        let criteria
        if ((name && city) || (name || city)) {
            if (name && city) {
                criteria = 'WHERE City = "' + city + '"' + ' AND Name = "' + name + '"'
            }
            else if (name) {
                criteria = 'WHERE  Name = "' + name + '"'
            }
            else{
                criteria = 'WHERE  City = "' + city + '"'
            }
        } else {
            criteria = ''
        }
        if (criteria != '') {
            logger.log("searchcriteria =" + criteria)
        }

        connection.query('SELECT * FROM studenthome ' + criteria, function (error, results, fields) {


            callback(results, error)
        })
    },

    getWithId(ID, callback) {
        connection.query('SELECT * FROM studenthome where id=' + ID, function (error, results, fields) {
            logger.log('results: ', results)
            
            callback(results, error)
        })
    },

    getMealWithId(ID, callback) {
        connection.query('SELECT * FROM meal where StudenthomeID=' + ID, function (error, results, fields) {
            logger.log('results: ', results)
            callback(results, error)
        })
    },

    getMealWithIdFromId(ID, mealID, callback) {
        connection.query('SELECT * FROM meal where StudenthomeID= ' + ID + ' and ID = ' + mealID, function (error, results, fields) {
            logger.log('results: ', results)
            callback(results, error)
        })
    },

    getParticipants(mealID, callback) {
        connection.query('SELECT * FROM participants where MealID = ' + mealID, function (error, results, fields) {
            logger.log('Participants: ', results)
            callback(results, error)
        })
    },

    getUserById(userID, callback) {
        connection.query('SELECT ID, First_Name, Email,	Student_Number FROM user where id = ' + userID, function (error, results, fields) {
            callback(results, error)
        })
    },

    signUpForMeal(userId, studenthomeID, mealID, callback) {
        var time = new Date()
        let sqlQuery = 'INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES (?,?,?,?)'
        connection.query(
            sqlQuery, [userId, studenthomeID, mealID, time],
            function (error, result, fields) {
            callback(result, error)
            })
    },

    addMeal(userID, homeIdGiven, req, callback) {
        logger.log("admeal activated")
        const { Name, Description, Ingredients, Allergies, OfferedOn, Price, MaxParticipants } = req.body
        var time = new Date()

        let sqlQuery = 'INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`,`CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES (?,?,?,?,?,?,?,?,?,?)'
        connection.query(
            sqlQuery, [Name, Description, Ingredients, Allergies, time, OfferedOn, Price, userID, homeIdGiven, MaxParticipants],
            function (error, results, fields) {
                if (error == undefined) {
                    connection.query('SELECT * FROM meal where id=' + results["insertId"], function (err, result, fields) {

                        callback(result, err)
                    })
                } else {

                    callback(undefined, error)
                }
            })

    },

    changeMeal(mealIdGiven, req, callback) {
        logger.log("changeMeal activated")
        const { Name, Description, Ingredients, Allergies, OfferedOn, Price, StudenthomeID, MaxParticipants } = req
        var time = new Date()
        let sqlQuery = 'UPDATE `meal` SET Name = ?, Description = ?, Ingredients = ?, Allergies =? ,CreatedOn = ? , OfferedOn = ? , Price = ? ,  StudenthomeID = ? , MaxParticipants = ? WHERE ID = ?'
        connection.query(
            sqlQuery, [Name, Description, Ingredients, Allergies, time, OfferedOn, Price, StudenthomeID, MaxParticipants, mealIdGiven],
            function (error, results, fields) {
                if (error == undefined) {
                    connection.query('SELECT * FROM meal where id=' + mealIdGiven, function (err, result, fields) {

                        callback(result, err)
                    })
                } else {

                    callback(undefined, error)
                }
            })

    },

    deleteMeal(ID, StudenthomeID, callback) {
        connection.query('SELECT * FROM meal where id=' + ID + ' and StudenthomeID = ' + StudenthomeID, function (error, results, fields) {
            if (error != undefined) {
                callback(undefined, error)
            }
            else {
                connection.query('DELETE FROM meal where id=' + ID + ' and StudenthomeID = ' + StudenthomeID, function (err, result, fields) {

                    callback(results, err)
                })
            }

        })
    },

    deleteUser(ID, callback) {
        connection.query('SELECT ID, First_Name, Last_Name, Email, Student_Number  FROM user where id=' + ID, function (error, results, fields) {
            if (error != undefined) {
                callback(undefined, error)
            }
            else {
                connection.query('DELETE FROM user where id=' + ID, function (err, result, fields) {
                    if (err != undefined) {
                        callback(undefined, err)
                    } else {
                        connection.query('DELETE FROM participants where UserID=' + ID, function (errr, result, fields) {
                            callback(results, err)
                        })
                    }

                })
            }
        })
    },

    changeUser(userIdGiven, req, callback) {
        const { firstname, lastname, email, password } = req.body
        logger.log(firstname, lastname, email, password)
        let sqlQuery = 'UPDATE user SET First_Name = ?, Last_Name = ?, email = ?, password = ?  WHERE ID = ?'
        bcrypt.hash(password, saltRounds, function (hashErr, encryptedPassword) {
            if (hashErr) {
                next({ error: hashErr.message, message: 'password can not be encrypted', errCode: 400 })
            } else {
                connection.query(
                    sqlQuery, [firstname, lastname, email, encryptedPassword, userIdGiven],
                    function (error, results, fields) {
                        callback(results, error)
                    })
            }
        });
    },

    changeHome(homeIdGiven, userid, streetName, houseNumber, postalcode, phonenumber, name, city, callback) {
        let sqlQuery = 'UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, UserID = ?, Postal_Code = ?, Telephone = ?, City = ?  WHERE ID = ?'//and userID = userid or check userid first
        connection.query(
            sqlQuery, [name, streetName, parseInt(houseNumber, 10), userid, postalcode, phonenumber, city, homeIdGiven],
            function (error, results, fields) {
                if (error == undefined) {
                    connection.query('SELECT * FROM studenthome where id=' + homeIdGiven, function (err, result, fields) {

                        //logger.log("ID: "+result.ID)
                        callback(result, err)
                    })
                } else {
                    callback(undefined, error)
                }
            })
    },

    addHome(userid, { streetName, houseNumber, postalcode, phonenumber, name, city }, callback) {
        let sqlQuery = 'INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES (?,?,?,?,?,?,?)'
        connection.query(
            sqlQuery, [name, streetName, parseInt(houseNumber, 10), userid, postalcode, phonenumber, city],
            function (error, results, fields) {
                if (error == undefined) {
                    connection.query('SELECT * FROM studenthome where id=' + results["insertId"], function (err, result, fields) {

                        callback(result, err)
                    })
                } else {
                    callback(undefined, error)
                }
            })
    },

    deleteHome(ID, UserID, callback, next) {
        connection.query('SELECT * FROM studenthome where id=' + ID, function (error, results, fields) {

            if (error != undefined) {
                callback(undefined, error)
            }
            else {
                if (results.length > 0 && results[0].UserID != UserID) {
                    callback(401, undefined)
                } else {
                    connection.query('DELETE FROM studenthome where id=' + ID, function (err, result, fields) {

                        callback(results, err)
                    })
                }

            }

        })
    },

    signOfForMeal(UserID, StudenthomeID, MealID, callback, next) {
        connection.query('DELETE FROM participants where UserID=' + UserID + ' and StudenthomeID = ' + StudenthomeID + ' and MealID = ' + MealID, function (err, result, fields) {

            callback(result.affectedRows, err)
        })
    }
}
