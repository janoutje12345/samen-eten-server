const assert = require('assert')
const { name } = require('ci-info')
const config = require('../config/tracerConfig')
const { route } = require('../routes/house-routes')
const { routes } = require('../../server')
const logger = config.logger
const database = require('../storage/database')
const databaseControl = require('../storage/databaseController')


module.exports = {
    getUser(req, res, next) {
        const userId = req.params.userId
        logger.log("getuser invoked")
        logger.log("with ID")
        logger.log(userId)
        databaseControl.getUserById(userId, (results, err) => {
            if (err == undefined) {
                if (results.length > 0) {
                    res.status(200)
                    res.json(results[0])
                } else {
                    next({ error: "user does not exist", errCode: 422 })
                }
            } else {
                //sending error message and error code to the next error handler
                next({ error: err.message, errCode: 422 })
            }
        })
    },
    getUserByOwnId(req, res, next) {
        databaseControl.getUserById(req.userId, (results, err) => {
            if (err == undefined) {
                    res.status(200)
                    res.json(results[0])
            } else {
                //sending error message and error code to the next error handler
                next({ error: err.message, errCode: 422 })
            }
        })
    },
    updateUser(req, res, next) {
        
        logger.log("update user invoked")
        databaseControl.changeUser(req.userId, req, (results, err) => {
            if (err == undefined) {
                next()
            } else {
                //sending error message and error code to the next error handler
                next({ message: err.message, errCode: 422 })
            }
        })
    },
    deleteUser(req, res, next) {
        databaseControl.deleteUser(req.userId, (results, err) => {
            if (err == undefined) {
                if (results.length > 0) {
                    res.status(200)
                    let returnResult = {
                        deleted: results[0]
                    }
                    res.json(returnResult)
                } else {
                    next({ error: "user does not exist", errCode: 422 })
                }
            } else {
                //sending error message and error code to the next error handler
                next({ error: err.message, errCode: 422 })
            }
        })
    }



}