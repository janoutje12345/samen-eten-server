
const assert = require('assert')
const { name } = require('ci-info')
const config = require('../config/tracerConfig')
const { route } = require('../routes/house-routes')
const { routes } = require('../../server')
const logger = config.logger
const database = require('../storage/database')
const databaseControl = require('../storage/databaseController')

module.exports = {

    //UC-301.1 maaltijd aanmaken, validatie
    validateMeal(req, res, next) {

        logger.log("Validating meal")
        logger.log(req.body)
        const homeIdGiven = req.params.homeId
        try {
            const { Name, Description, Ingredients, Allergies, OfferedOn, Price, MaxParticipants } = req.body
            assert(typeof Name === "string", "Name is missing")
            assert(typeof Description === 'string', 'Description is missing')
            assert(typeof Ingredients === 'string', 'Ingredients is missing')
            assert(typeof Allergies === 'string', 'Allergies is missing')
            assert(typeof OfferedOn === 'string', 'OfferedOn is missing')
            assert(typeof Price === 'number', 'Price is missing')
            assert(typeof homeIdGiven === 'string', 'StudenthomeID is missing')
            assert(typeof MaxParticipants === 'number', 'MaxParticipants is missing')
            const offerDateInMS = Date.parse(OfferedOn)
            var time = new Date().getTime();
            if (Number.isNaN(offerDateInMS)) {
                next({ error: "date is not in right format", errCode: 400 })
            }
            if (offerDateInMS < time) {
                next({ error: "date is in the past", errCode: 400 })
            }
            next()
        } catch (err) {
            logger.log('Meal is invalid: ', err.message)
            next({ error: err.message, errCode: 400 })
        }
    },

    //UC-301 Maaltijd aanmaken
    addMeal(req, res, next) {
        const homeIdGiven = req.params.homeId
        logger.log("asking for home with id: " + homeIdGiven)
        databaseControl.getWithId(homeIdGiven, (result, err) => {
            if (err == undefined && result.length != 0) {
        
                databaseControl.addMeal(req.userId, homeIdGiven, req, (results, error) => {
                    if (error == undefined) {
                        if (results == undefined) {
                            next({ error: "did not work, try again", errCode: 422 })
                        } else {
                            res.status(200)
                            let returnResult = {
                                result: results[0]
                            }
                            res.json(returnResult)
                        }
                    } else {
                        //sending error message and error code to the next error handler
                        next({ error: error.message, errCode: 422 })
                    }
                })
            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist", errCode: 422 })
            }
        })
    },

    //UC-302 Maaltijd wijzigen validatie
    changeMealValidator(req, res, next) {
        const name = req.body["Name"]
        const description = req.body["Description"]
        const offeredDate = req.body["OfferedDate"]
        const price = req.body["Price"]
        const allergies = req.body["Allergies"]
        const ingredients = req.body["Ingredients"]

        try {
            if (name != undefined) {
                assert(typeof name === "string", "name is not a String")
            }
            if (description != undefined) {
                assert(typeof description === 'string', 'description is not a String')
            }
            if (offeredDate != undefined) {
                assert(typeof offeredDate === 'string', 'offeredDate is not a String')
            }
            if (price != undefined) {
                assert(typeof price === 'number', 'price is not a String')
            }
            if (allergies != undefined) {
                assert(typeof allergies === 'string', 'allergies is not a String')
            }
            if (ingredients != undefined) {
                assert(typeof ingredients === 'string', 'ingredients is not a String')
            }
            next()
            //is send to server.js
        } catch (err) {
            logger.log('Meal is invalid: ', err.message)
            next({ error: err.message, errCode: 400 })
        }
    },

    //UC-302 Maaltijd wijzigen
    ChangeMeal(req, res, next) {
        logger.log('ChangeMeal  activated')

        const userId = req.userId
        const homeIdGiven = req.params.homeId
        const mealIdGiven = req.params.mealId
        const reqName = req.body["Name"]
        const recDescription = req.body["Description"]
        const reqOfferedDate = req.body["OfferedDate"]
        const reqPrice = req.body["Price"]
        const reqAllergies = req.body["Allergies"]
        const reqIngredients = req.body["Ingredients"]
        databaseControl.getMealWithIdFromId(homeIdGiven, mealIdGiven, (result, error) => {
            if (error == undefined && result != undefined && result.length > 0) {

                if (result[0].UserID != userId) {
                    next({ error: "You do not have access to this item", errCode: 401 })
                } else {
                    if (reqName != undefined) {
                        result[0].Name = reqName
                    }
                    if (recDescription != undefined) {
                        result[0].Description = recDescription
                    }
                    if (reqOfferedDate != undefined) {
                        result[0].OfferedOn = reqOfferedDate
                    }
                    if (reqPrice != undefined) {
                        result[0].Price = reqPrice
                    }
                    if (reqAllergies != undefined) {
                        result[0].Allergies = reqAllergies
                    }
                    if (reqIngredients != undefined) {
                        result[0].Ingredients = reqIngredients
                    }
                    databaseControl.changeMeal(mealIdGiven, result[0], (changeResult, err) => {
                        if (err == undefined && changeResult != undefined && changeResult.length > 0) {
                            res.status(200)
                            let returnResult = {
                                result: changeResult[0]
                            }
                            res.json(returnResult)
                        } else {
                            //sending error message and error code to the next error handler
                            next({ error: "meal does not exist", errCode: 404 })
                        }
                    })
                }



            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist or meal does not exist", errCode: 404 })
            }
        })
    },

    //UC-303 Lijst van maaltijden opvragen
    getMealsFromId(req, res, next) {
        logger.log('getMealsFromId  activated')
        // app.get('/api/select/byid/:houseId', (req, res, next) => {
        //req.params gives the given parameters, here you see the parameter 'homeId' selected
        const homeIdGiven = req.params.homeId
        databaseControl.getMealWithId(homeIdGiven, (result, err) => {
            if (err == undefined && result != undefined && result.length > 0) {
                res.status(200)
                let results = {
                    result: result
                }
                res.json(results)
            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist or has no meals", errCode: 422 })
            }
        })
    },

    //UC-304 Details van een maaltijd opvragen
    getMealFromIdWithId(req, res, next) {
        logger.log('getMealsFromIdWithId  activated')
        // app.get('/api/select/byid/:houseId', (req, res, next) => {
        //req.params gives the given parameters, here you see the parameter 'homeId' selected
        const homeIdGiven = req.params.homeId
        const mealIdGiven = req.params.mealId
        databaseControl.getMealWithIdFromId(homeIdGiven, mealIdGiven, (result, err) => {
            if (err == undefined && result != undefined && result.length > 0) {
                res.status(200)
                res.json(result)
            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist or meal does not exist", errCode: 404 })
            }
        })
    },
    //UC-404 not found
    logTillYouDrop(req, res, next) {
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        logger.log("log till you drop")
        next({ error: "log till you drop", errCode: 422 })
        
    },
    
    //UC-305 Maaltijd verwijderen
    deleteMealFromIdWithId(req, res, next) {
        logger.log('getMealsFromIdWithId  activated')
        const homeIdGiven = req.params.homeId
        const mealIdGiven = req.params.mealId
        databaseControl.deleteMeal(mealIdGiven, homeIdGiven, (result, err) => {
            if (err == undefined && result != undefined && result.length > 0) {
                res.status(200)
                res.json(result)
            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist or meal does not exist", errCode: 422 })
            }
        })
    },

    //UC-401 Aanmelden voor maaltijd
    signUpForMeal(req, res, next) {
        const homeIdGiven = req.params.homeId
        const mealIdGiven = req.params.mealId
        const userId = req.userId
        databaseControl.getMealWithIdFromId(homeIdGiven, mealIdGiven, (result, err) => {
            if (err == undefined && result != undefined && result.length > 0) {
                databaseControl.getParticipants(mealIdGiven, (results, errr) => {
                    if (errr == undefined) {
                        logger.log(result[0].MaxParticipants + " should be bigger than " + results.length)
                        if (result[0].MaxParticipants > results.length) {
                            databaseControl.signUpForMeal(userId, homeIdGiven, mealIdGiven, (resultsFromSignup, error) => {
                                if (error == undefined || resultsFromSignup) {
                                    databaseControl.getParticipants(mealIdGiven, (finalResults, errorFromgetParticipants) => {
                                        if (errorFromgetParticipants == undefined) {
                                            let returnResult ={
                                                result: finalResults
                                            }
                                            res.status(200)
                                            res.json(returnResult)
                                        } else {
                                            //sending error message and error code to the next error handler
                                            next({ error: "Could not get the list of people", errCode: 422 })
                                        }
                                    })
                                } else {
                                    //sending error message and error code to the next error handler
                                    next({ error: "You are already seated", errCode: 409 })
                                }
                            })
                        } else {
                            next({ error: "All seats are already taken", errCode: 422 })
                        }
                    } else {
                        //sending error message and error code to the next error handler
                        next({ error: "house does not exist or meal does not exist", errCode: 422 })
                    }
                })
            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist or meal does not exist", errCode: 422 })
            }
        })
    },

    //UC-402 Afmelden voor maaltijd
    signOfForMeal(req, res, next) {
        const homeIdGiven = req.params.homeId
        const mealIdGiven = req.params.mealId
        const userId = req.userId
        databaseControl.signOfForMeal(userId, homeIdGiven, mealIdGiven, (result, err) => {
            if (err == undefined) {
                if (result == 1) {
                    databaseControl.getParticipants(mealIdGiven, (finalResults, errorFromgetParticipants) => {
                        if (errorFromgetParticipants == undefined) {
                            res.status(200)
                            let results = {
                                "Reservation canceled": finalResults
                            }
                            res.json(results)
                        } else {
                            //sending error message and error code to the next error handler
                            next({ error: "There are no people seated", errCode: 422 })
                        }
                    })
                } else {
                    next({ error: "No reservation was canceled, check list to see if you are seated", errCode: 422 })
                }

            } else {
                //sending error message and error code to the next error handler
                next({ error: "Something went wrong", errCode: 422 })
            }
        })


    },

    //UC-403 Lijst van deelnemers opvragen
    getParticipants(req, res, next) {
        const mealIdGiven = req.params.mealId
        
        databaseControl.getParticipants(mealIdGiven, (finalResults, errorFromgetParticipants) => {
            if (errorFromgetParticipants == undefined) {
                let returnResult ={
                    result: finalResults
                }
                res.status(200)
                res.json(returnResult)
            } else {
                //sending error message and error code to the next error handler
                next({ error: "Could not get the list of people for the given mealId", errCode: 422 })
            }
        })
    },

    //UC-404Details van deelnemer opvragen
    getParticipant(req, res, next) {
        const mealIdGiven = req.params.mealId
        const participantId = req.params.participantId
        let isHeThere = false;
        let i = 0
        databaseControl.getParticipants(mealIdGiven, (finalResults, errorFromgetParticipants) => {
            if (errorFromgetParticipants == undefined && finalResults.length != 0) {
                for (i = 0; i < finalResults.length; i++) {
                    if (finalResults[i].UserID == participantId) {
                        isHeThere = true
                    }
                }
                if (!isHeThere) {
                    next({ error: "User is not found at this table", errCode: 422 })
                } else {
                    databaseControl.getUserById(participantId, (results, err) => {
                        if (err == undefined) {
                            res.status(200)
                            res.json(results[0])
                        } else {
                            //sending error message and error code to the next error handler
                            next({ error: err.message, errCode: 422 })
                        }
                    })
                }
            } else {
                //sending error message and error code to the next error handler
                next({ error: "There are no people at this table", errCode: 422 })
            }
        })
    },

}
