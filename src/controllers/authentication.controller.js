
// Authentication controller

const assert = require('assert')
const config = require('../config/tracerConfig')
const logger = config.logger
const jwt = require('jsonwebtoken')
const pool = require('../storage/database').pool
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwtSecretKey = require('../config/config').jwtSecretKey

module.exports = {
  login(req, res, next) {
    logger.log("email is: " + validateEmail(req.body.email))
    if (!validateEmail(req.body.email)) {
      assert(
        typeof req.body.email === 'number',
        'email not correct.'
      )
    }
    logger.log("password is: " + CheckPassword(req.body.password))
    if (!CheckPassword(req.body.password)) {
      assert(
        typeof req.body.password === 'number',
        'password needs to be 7 to 15 characters which contain at least one numeric digit and a special character.'
      )
    }
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error('Error getting connection from pool')
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() })
      }
      if (connection) {
        // 1. Kijk of deze useraccount bestaat.
        connection.query(
          'SELECT `ID`, `Email`, `Password`, `First_Name`, `Last_Name`, `Student_Number` FROM `user` WHERE `Email` = ?',
          [req.body.email],
          (err, rows, fields) => {
            connection.release()
            if (err) {
              logger.error('Error: ', err.toString())
              res.status(500).json({
                error: err.toString(),
                datetime: new Date().toISOString()
              })
            } else {
              // 2. Er was een resultaat, check het password.
              logger.info('Result from database: ')
              logger.info(rows)
              if (
                rows && rows.length === 1 //result = false or true
              ) {
                bcrypt.compare(req.body.password, rows[0].Password, function (err, result) {
                  if (err) {
                    next({ error: 'password unable to uncrypt', errCode: 400 })
                  } else {
                    if (
                      result //result = false or true
                    ) {
                      logger.info('passwords DID match, sending valid token')
                      // Create an object containing the data we want in the payload.
                      const payload = {
                        id: rows[0].ID
                      }
                      // Userinfo returned to the caller.
                      const userinfo = {
                        id: rows[0].ID,
                        firstName: rows[0].First_Name,
                        lastName: rows[0].Last_Name,
                        Student_Number: rows[0].Student_Number,
                        emailAdress: rows[0].Email,
                        token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
                      }
                      logger.debug('Logged in, sending: ', userinfo)
                      res.status(200).json(userinfo)
                    } else {
                      logger.info('User not found or password invalid')
                      next({ error: 'User not found or password invalid.', errCode: 400 })

                    }
                  }
                });
              } else {
                logger.info('User not found or password invalid')
                next({ error: 'User not found or password invalid.', errCode: 400 })
              }
            }
          }
        )
      }
    })
  },

  //
  //
  //
  validateLogin(req, res, next) {
    // Verify that we receive the expected input
    try {
      assert(typeof req.body.email === 'string', 'email must be a string.')
      assert(
        typeof req.body.password === 'string',
        'password must be a string.'
      )
      next()
    } catch (ex) {
      next({ error: ex.message, message: 'user not found.', errCode: 400 })

    }
  },

  //
  //
  //
  register(req, res, next) {
    logger.info('register')
    logger.info(req.body)

    /**
     * Query the database to see if the email of the user to be registered already exists.
     */
    pool.getConnection((err, connection) => {
      if (err) {
        res
          .status(500)
          .json({ error: ex.toString(), datetime: new Date().toISOString() })
      }
      if (connection) {
        let { firstname, lastname, email, Student_Number, password } = req.body

        bcrypt.hash(password, saltRounds, function (hashErr, encryptedPassword) {
          if (hashErr) {
            next({ error: hashErr.message, message: 'password can not be encrypted', errCode: 400 })
          } else {
            connection.query(
              'INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)',
              [firstname, lastname, email, Student_Number, encryptedPassword],
              (insertErr, rows, fields) => {
                connection.release()
                if (insertErr) {
                  // When the INSERT fails, we assume the user already exists
                  
                  next({ error: insertErr.message, message: 'This email has already been taken.', errCode: 400 })

                } else {
                  logger.trace(rows)
                  // Create an object containing the data we want in the payload.
                  // This time we add the id of the newly inserted user
                  const payload = {
                    id: rows.insertId
                  }
                  // Userinfo returned to the caller.
                  const userinfo = {
                    id: rows.insertId,
                    firstName: firstname,
                    lastName: lastname,
                    Student_Number: Student_Number,
                    emailAdress: email,
                    token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
                  }
                  
                  res.status(200).json(userinfo)
                }
              }
            )
          }
        });
      }
    })
  },

  validateRegister(req, res, next) {
    // Verify that we receive the expected input
    try {
      assert(
        typeof req.body.firstname === 'string',
        'firstname must be a string.'
      )
      assert(
        typeof req.body.lastname === 'string',
        'lastname must be a string.'
      )
      assert(typeof req.body.email === 'string', 'email must be a string.')
      logger.log("email is: " + validateEmail(req.body.email))
      if (!validateEmail(req.body.email)) {
        assert(
          typeof req.body.email === 'number',
          'email not correct.'
        )
      }
      assert(
        typeof req.body.Student_Number === 'string',
        'Student_Number must be a string.'
      )
      assert(
        typeof req.body.password === 'string',
        'password must be a string.'
      )
      logger.log("password is: " + CheckPassword(req.body.password))
      if (!CheckPassword(req.body.password)) {
        assert(
          typeof req.body.password === 'number',
          'password needs to be 7 to 15 characters which contain at least one numeric digit and a special character.'
        )
      }

      next()
    } catch (ex) {
      logger.debug('validateRegister error: ', ex.toString())
      next({ error: ex.toString(), errCode: 400 })

    }
  },

  //
  //
  //
  validateToken(req, res, next) {
    logger.info('validateToken called')
    // logger.trace(req.headers)
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization
    if (!authHeader) {
      logger.warn('Authorization header missing!')
      next({ error: 'Authorization header missing!', errCode: 401 })
      
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length)
      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn('Not authorized')
          next({ error: 'Not authorized', errCode: 401 })

        }
        if (payload) {
          logger.debug('token is valid', payload)
          // User heeft toegang. Voeg UserId uit payload toe aan
          // request, voor ieder volgend endpoint.
          req.userId = payload.id
          next()
        }
      })
    }
  },

  renewToken(req, res, next) {
    logger.debug('renewToken')

    pool.getConnection((err, connection) => {
      if (err) {
        logger.error('Error getting connection from pool')
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() })
      }
      if (connection) {
        // 1. Kijk of deze useraccount bestaat.
        connection.query(
          'SELECT * FROM `user` WHERE `ID` = ?',
          [req.userId],
          (err, rows, fields) => {
            connection.release()
            if (err) {
              logger.error('Error: ', err.toString())
              res.status(500).json({
                error: err.toString(),
                datetime: new Date().toISOString()
              })
            } else {
              // 2. User gevonden, return user info met nieuw token.
              // Create an object containing the data we want in the payload.
              const payload = {
                id: rows[0].ID
              }
              // Userinfo returned to the caller.
              const userinfo = {
                id: rows[0].ID,
                firstName: rows[0].First_Name,
                lastName: rows[0].Last_Name,
                emailAdress: rows[0].Email,
                token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
              }
              logger.debug('Sending: ', userinfo)
              res.status(200).json(userinfo)
            }
          }
        )
      }
    })
  }
}
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function CheckPassword(paswd) {
  const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
  return re.test(paswd);
}