const assert = require('assert')
const config = require('../config/tracerConfig')
const { post } = require('../../server')
const logger = config.logger
const databaseControl = require('../storage/databaseController')
const mysql = require('mysql')
module.exports = {

    //UC-202 Overzicht van studentenhuizen
    getAll(req, res, next) {
        logger.log("getAll aangeroepen")
        let city
        let name
        logger.log(req.query.city)
        if (req.query.name) {
            name = req.query.name
            logger.log("searching for " + name)
        }
        else if (req.query.city) {
            city = req.query.city
            logger.log("searching for " + city)
        }

        databaseControl.getAll(name, city, (result, err) => {
            if (err == undefined) {
                res.status(200)
                let returnResult = {
                    result: result
                }
                res.json(returnResult)
            } else {
                //sending error message and error code to the next error handler
                next({ error: "info does not exist", errCode: 422 })
            }
        })
    },

    //UC-203 Details van studentenhuis via id
    getHouseById(req, res, next) {
        logger.log('houseController getHouseById activated')
        // app.get('/api/select/byid/:houseId', (req, res, next) => {
        //req.params gives the given parameters, here you see the parameter 'homeId' selected
        const homeIdGiven = req.params.homeId
        logger.log("select called for nr " + homeIdGiven)
        databaseControl.getWithId(homeIdGiven, (result, err) => {
            if (err == undefined && result.length > 0) {
                res.status(200)
                let returnResult = {
                    result: result
                }
                res.json(returnResult)
            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist", errCode: 404 })
            }
        })
    },

    //UC-201.1 Maak studentenhuis, validatie 
    validateHouse(req, res, next) {
        logger.log("Validating house")
        logger.log(req.body)
        let validHouse = true
        try {
            const { streetName, houseNumber, postalcode, phonenumber, name, city } = req.body
            assert(typeof streetName === "string", "address is missing")
            assert(typeof houseNumber === 'number', 'houseNumber is missing')
            assert(typeof postalcode === 'string', 'postalcode is missing')
            assert(typeof phonenumber === 'string', 'phonenumber is missing')
            assert(typeof name === 'string', 'name is missing')
            assert(typeof city === 'string', 'city is missing')

            if (!validatePostalcode(postalcode)) {
                validHouse = false
                next({ error: "postalcode not correct, needs format: '1234AB', but got " + postalcode.toUpperCase(), errCode: 400 })
            }
            if (!validatePhoneNumber(phonenumber)) {
                validHouse = false
                next({ error: "phonenumber not correct, needs format: '0612345678', but got " + phonenumber, errCode: 400 })
            }
            if (validHouse) {
                logger.log("House data is valid")
            }
            next()
        } catch (err) {
            logger.log('House is invalid: ', err.message)
            next({ error: err.message, errCode: 400 })
        }
    },

    //UC-201 Maak studentenhuis
    addHouse(req, res, next) {
        databaseControl.addHome(req.userId, req.body, (result, err) => {
            if (err == undefined && result) {
                res.status(200)
                res.json(result)
            } else {
                //sending error message and error code to the next error handler
                if (err) {
                    next({ error: err, errCode: 422 })
                } else {
                    next({ error: "Something went wrong, please try again", errCode: 422 })
                }

            }
        })
    },

    //UC-205 Studentenhuis verwijderen
    deleteHouse(req, res, next) {
        const homeIdGiven = req.params.homeId
        logger.log("select called for nr " + homeIdGiven)
        logger.log("UserID = " + req.userId)
        databaseControl.deleteHome(homeIdGiven, req.userId, (result, err) => {
            if (result == 401) {
                next({ error: "User is not authorised for this house", errCode: 401 })
            } else {
                if (err == undefined && result.length > 0) {
                    logger.log("home deleted")

                    let returnResult = {
                        result: result
                    }
                    res.json(returnResult)
                } else {
                    //sending error message and error code to the next error handler
                    next({ error: "house does not exist", errCode: 404 })
                }
            }
        })
    },

    //UC-204Studentenhuis wijzigen, validatie
    validateChanges(req, res, next) {
        //check if the info that is give is correct, does not return a error if data is missing because the info is optional
        logger.log("Validating changes")
        logger.log(req.body)
        let validHouse = true
        try {
            const { streetName, houseNumber, postalcode, phonenumber, name, city } = req.body
            if (streetName) {
                assert(typeof streetName === "string", "address is not a String")
            }
            if (houseNumber) {
                assert(typeof houseNumber === 'number', 'houseNumber is not a number')
            }
            if (postalcode) {
                assert(typeof postalcode === 'string', 'postalcode is not a String')
                if (!validatePostalcode(postalcode)) {
                    validHouse = false
                    next({ error: "postalcode not correct, needs format: '1234AB', but got " + postalcode.toUpperCase(), errCode: 400 })
                }
            }
            if (phonenumber) {
                assert(typeof phonenumber === 'string', 'phonenumber is not a String')
                if (!validatePhoneNumber(phonenumber)) {
                    validHouse = false
                    next({ error: "phonenumber not correct, needs format: '0612345678', but got " + phonenumber, errCode: 400 })
                }
            }
            if (name) {
                assert(typeof name === 'string', 'name is not a String')
            }
            if (city) {
                assert(typeof city === 'string', 'city is not a String')
            }
            next()
        } catch (err) {
            logger.log('data is invalid: ', err.message)
            next({ error: err.message, errCode: 400 })
        }
    },

    //UC-204Studentenhuis wijzigen
    putHouseById(req, res, next) {
        logger.log('houseController putHouseById activated')
        // app.get('/api/select/byid/:houseId', (req, res, next) => {
        //req.params gives the given parameters, here you see the parameter 'homeId' selected
        const homeIdGiven = req.params.homeId
        logger.log("select called for nr " + homeIdGiven)
        databaseControl.getWithId(homeIdGiven, (result, err) => {
            if (err == undefined && result.length > 0) {
                const { streetName, houseNumber, postalcode, phonenumber, name, city } = req.body
                if (streetName != undefined) {
                    result[0].Address = streetName
                }
                
                if (houseNumber != undefined) {
                    result[0].House_Nr = houseNumber
                }
                if (postalcode != undefined) {
                    result[0].Postal_Code = postalcode
                }
                if (phonenumber != undefined) {
                    result[0].Telephone = phonenumber
                }
                if (name != undefined) {
                    result[0].Name = name
                }
                if (city != undefined) {
                    result[0].City = city
                }
                logger.log("Gives home ID " + homeIdGiven)
                logger.log("Gives userID " + req.userId)

                if (result[0].UserID != req.userId) {

                    next({ error: "You do not have permission for this house", errCode: 422 })
                } else {
                    databaseControl.changeHome(homeIdGiven, req.userId, result[0].Address, result[0].House_Nr, result[0].Postal_Code, result[0].Telephone, result[0].Name, result[0].City, (newResult, error) => {
                        if (error != undefined) {
                            next({ error: "house not updated because " + error.message, errCode: 422 })
                        }

                        res.status = 200
                        res.json(newResult)
                    })
                }
            } else {
                //sending error message and error code to the next error handler
                next({ error: "house does not exist", errCode: 422 })
            }
        })
    }

}

//UC-201.2 Maak studentenhuis, validatie telefoonnummer
function validatePhoneNumber(number) {
    //phoneo is a format
    var phoneno = /^\d{10}$/;
    logger.log('validating phone number, number = ', phoneno.test(number))
    return phoneno.test(number)
}

//UC-201.3 Maak studentenhuis, validatie postcode
function validatePostalcode(postalcode) {
    //rege is a format
    var rege = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
    logger.log('validating postalcode, postalcode = ', rege.test(postalcode))
    //rege.test checks if postalcode matches dutch standards
    return rege.test(postalcode)
}


