const express = require('express')
const userControl = require('../controllers/userControler')
const router = express.Router()
const config = require('../config/tracerConfig')
const logger = config.logger
const AuthController = require('../controllers/authentication.controller')

router.use(function timeLog(rec, res, next){
    logger.log("user-routes called upon")
    next()
})

router.get('/user/:userId', userControl.getUser)
router.get('/user/me', AuthController.validateToken, userControl.getUserByOwnId)

router.put('/user/update', AuthController.validateToken, AuthController.validateLogin, userControl.updateUser, userControl.getUserByOwnId)

router.delete('/user/me', AuthController.validateToken, userControl.deleteUser)


module.exports=router