//
// Authentication routes
//
const express = require('express')
const router = express.Router()
const AuthController = require('../controllers/authentication.controller')
const config = require('../config/tracerConfig')
const logger = config.logger

router.post('/login', AuthController.validateLogin, AuthController.login)
router.post('/register', AuthController.validateRegister, AuthController.register)
router.get('/validate', AuthController.validateToken, AuthController.renewToken)

module.exports = router 
