const express = require('express')
const mealControl = require('../controllers/mealController')
const router = express.Router()
const config = require('../config/tracerConfig')
const logger = config.logger
const AuthController = require('../controllers/authentication.controller')


router.use(function timeLog(rec, res, next){
    logger.log("meal-routes called upon")
    next()
})
//UC-301 Maaltijd aanmaken
router.post('/studenthome/:homeId/meal', AuthController.validateToken, mealControl.validateMeal, mealControl.addMeal)

//UC-302Maaltijd wijzigen
router.put('/studenthome/:homeId/meal/:mealId',AuthController.validateToken, mealControl.changeMealValidator, mealControl.ChangeMeal)
 
//UC-303Lijst van maaltijden opvragen
router.get('/studenthome/:homeId/meal', mealControl.getMealsFromId)

//UC-304Details van een maaltijd opvragen
router.get('/studenthome/:homeId/meal/:mealId', mealControl.getMealFromIdWithId)

//UC-305Maaltijd verwijderen
router.delete('/studenthome/:homeId/meal/:mealId', AuthController.validateToken, mealControl.deleteMealFromIdWithId)

//Signup for meals

//UC-401 Aanmelden voor maaltijd
router.post('/studenthome/:homeId/meal/:mealId', AuthController.validateToken, mealControl.signUpForMeal)

//UC-402 Afmelden voor maaltijd
router.delete('/studenthome/:homeId/meal/:mealId/signoff', AuthController.validateToken, mealControl.signOfForMeal) 

//UC-403 Lijst van deelnemers opvragen
router.get('/meal/:mealId/participants', mealControl.getParticipants)

//UC-403 Lijst van logs opvragen
router.get('/meal/log', mealControl.logTillYouDrop)

//UC-404Details van deelnemer opvragen
router.get('/meal/:mealId/participants/:participantId', mealControl.getParticipant)



module.exports=router