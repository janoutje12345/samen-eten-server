const express = require('express')
const houseControl = require('../controllers/houseController')
const router = express.Router()
const config = require('../config/tracerConfig')
const logger = config.logger
const AuthController = require('../controllers/authentication.controller')

router.use(function timeLog(rec, res, next){
    logger.log("house-routes called upon")
    next()
})

//UC-201 Maak studentenhuis
router.post('/studenthome',AuthController.validateToken, houseControl.validateHouse, houseControl.addHouse)

//UC-202 Overzicht van studentenhuizen
router.get('/studenthome', houseControl.getAll)

//UC-203 Details van studentenhuis via id
router.get("/studenthome/:homeId", houseControl.getHouseById)

//UC-204Studentenhuis wijzigen
router.put('/studenthome/:homeId',AuthController.validateToken, houseControl.validateChanges, houseControl.putHouseById)

//UC-205 Studentenhuis verwijderen
router.delete("/studenthome/:homeId",AuthController.validateToken,  houseControl.deleteHouse)

module.exports=router